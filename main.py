# Sorting Algorithms Provided by: https://realpython.com/sorting-algorithms-python/
import timeit
import itertools
from random import randint
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import make_interp_spline, BSpline


def insertion_sort(array):
  # Loop from the second element of the array until
  # the last element
  for i in range(1, len(array)):
    # This is the element we want to position in its
    # correct place
    key_item = array[i]

    # Initialize the variable that will be used to
    # find the correct position of the element referenced
    # by `key_item`
    j = i - 1

    # Run through the list of items (the left
    # portion of the array) and find the correct position
    # of the element referenced by `key_item`. Do this only
    # if `key_item` is smaller than its adjacent values.
    while j >= 0 and array[j] > key_item:
      # Shift the value one position to the left
      # and reposition j to point to the next element
      # (from right to left)
      array[j + 1] = array[j]
      j -= 1

    # When you finish shifting the elements, you can position
    # `key_item` in its correct location
    array[j + 1] = key_item

  return array


def merge(left, right):
  # If the first array is empty, then nothing needs
  # to be merged, and you can return the second array as the result
  if len(left) == 0:
    return right

  # If the second array is empty, then nothing needs
  # to be merged, and you can return the first array as the result
  if len(right) == 0:
    return left

  result = []
  index_left = index_right = 0

  # Now go through both arrays until all the elements
  # make it into the resultant array
  while len(result) < len(left) + len(right):
    # The elements need to be sorted to add them to the
    # resultant array, so you need to decide whether to get
    # the next element from the first or the second array
    if left[index_left] <= right[index_right]:
      result.append(left[index_left])
      index_left += 1
    else:
      result.append(right[index_right])
      index_right += 1

    # If you reach the end of either array, then you can
    # add the remaining elements from the other array to
    # the result and break the loop
    if index_right == len(right):
      result += left[index_left:]
      break

    if index_left == len(left):
      result += right[index_right:]
      break

  return result


def merge_sort(array):
  # If the input array contains fewer than two elements,
  # then return it as the result of the function
  if len(array) < 2:
    return array

  midpoint = len(array) // 2

  # Sort the array by recursively splitting the input
  # into two equal halves, sorting each half and merging them
  # together into the final result
  return merge(
    left=merge_sort(array[:midpoint]),
    right=merge_sort(array[midpoint:]))


# Run a sorting algorithm on a set of array lengths
#
# sorting_algo: The sorting algorithm being tested {"insertion_sort", "merge_sort"}
# lower_bound: The lower bound of the array length
# upper_bound: The upper bound of the array length
# step: The step to take when iterating, a step of 1 tests every value
# average_times: The list of average times being kept in memory
# iterations: The amount of arrays to test for every length
#
# This function adds the average time values to the average_times array
def time_sort(sorting_algo, lower_bound, upper_bound, step, average_times, iterations):
  for arr_length in range(lower_bound, upper_bound, step):

    # Define the timeit parameters
    setup = f"" \
            f"from main import insertion_sort, merge_sort\n" \
            f"from random import randint\n" \
            f"array = [randint(0, 1000) for i in range({arr_length})]"
    statement = f"{sorting_algo}(array)"

    # Time the execution of the function
    times = timeit.repeat(stmt=statement, setup=setup, repeat=iterations, number=1)

    # Add the average time it took to run the sort
    avg_time = sum(times) / len(times)
    average_times.append(avg_time)


# Test a sorting algorithm from array length 1 to 1000.
# This function uses a different number of arrays for
# every length range to increase accuracy and negate
# outliers.
# Saves the list of average times to a file
def run_sorting_algo(algo_name):
  average_times = []
  time_sort(algo_name, 1, 101, 1, average_times, 1000)
  time_sort(algo_name, 101, 201, 1, average_times, 200)
  time_sort(algo_name, 201, 301, 1, average_times, 100)
  time_sort(algo_name, 301, 401, 1, average_times, 50)
  time_sort(algo_name, 401, 501, 1, average_times, 25)
  time_sort(algo_name, 501, 1001, 10, average_times, 10)

  # Write the list of average times to a file
  file = open(f'{algo_name}.txt', 'w')
  for time in average_times:
    file.write(str(time) + "\n")
  file.close()


# Plot the time complexities of the sorting algorithms
# The default bounds will plot from array length 1 to
# 1000
def plot(lower_bound=0, upper_bound=550):
  range1 = range(1, 501)
  range2 = range(501, 1001, 10)
  times = [i for i in itertools.chain(range1, range2)][lower_bound:upper_bound]

  with open('insertion_sort.txt') as f:
    ins_averages = f.read().splitlines()
  ins_averages = [float(avg_time) for avg_time in ins_averages][lower_bound:upper_bound]

  with open('merge_sort.txt') as f:
    merge_averages = f.read().splitlines()
  merge_averages = [float(avg_time) for avg_time in merge_averages][lower_bound:upper_bound]

  plt.plot(times, ins_averages, color="Red", label="Insertion Sort")
  plt.plot(times, merge_averages, color="Blue", label="Merge Sort")
  plt.legend()

  plt.xlabel("Length of Array")
  plt.ylabel("Seconds")

  plt.tight_layout()
  plt.grid()
  plt.show()


def main():
  # run_sorting_algo("insertion_sort")
  # run_sorting_algo("merge_sort")
  plot()
  plot(0, 100)


if __name__ == "__main__":
  main()
